using System;
using FluentAssertions;
using LiteralNumberLib;
using Xunit;

namespace LiteralNumberTests
{
    public class NumberConverterFixture : IDisposable
    {
        public NumberConverter Converter { get; private set; }

        public NumberConverterFixture()
        {
            Converter = new NumberConverter();
        }
        
        public void Dispose()
        {
            Converter = null;
        }
    }
    
    public class NumberConverterTest : IClassFixture<NumberConverterFixture>
    {
        private NumberConverterFixture fixture;

        public NumberConverterTest(NumberConverterFixture fixture)
        {
            this.fixture = fixture;
        }
        
        [Fact]
        public void Number1ShouldReturnOne()
        {
            var result = fixture.Converter.GetStringFromNumber(1);

            result.Should().Be("One");
        }
    }
}
