dotnet sonarscanner begin /k:"LiteralNumbers" /d:sonar.host.url=http://localhost:9000 /d:sonar.cs.opencover.reportsPaths="./tests/LiteralNumberTests/TestResults/coverage.opencover.xml" /d:sonar.coverage.exclusions="**Tests*.cs" /d:sonar.cs.xunit.reportsPaths="./tests/LiteralNumberTests/TestResults/TestResults.xml"
dotnet build
dotnet test tests/LiteralNumberTests/LiteralNumberTests.csproj -l:xunit /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=./TestResults/
dotnet sonarscanner end
